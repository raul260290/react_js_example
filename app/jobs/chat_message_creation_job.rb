class ChatMessageCreationJob < ApplicationJob
  queue_as :default

  def perform(chat_message)
    ActionCable
      .server
      .broadcast('chat_channel', chat_message.to_json(only: [:id, :content, :built_at, :user_id, :user_email], methods: [:built_at, :user_email]))
  end

end
