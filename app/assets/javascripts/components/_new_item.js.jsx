var NewItem = React.createClass({
    handleClick() {
        let name = this.refs.name.value;
        let description = this.refs.description.value;

        $.ajax({
            url: '/api/v1/items',
            type: 'POST',
            data: { item: {name: name, description: description} },
            success: (item) => {
                this.props.handleSubmit(item)
            }
        });
    },

    render() {
        return (
            <div>
                <input ref='name' placeholder="Enter the name of the item"/>
                <input ref='description' type="Enter a description"/>
                <button onClick={this.handleClick}>Submit</button>
            </div>
        )
    }

});
