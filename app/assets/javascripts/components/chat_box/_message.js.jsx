class Message extends React.Component {
    render() {
        let sameUser = (this.props.message.user_id === this.props.userId);
        return (
            <div className={"direct-chat-msg " + (sameUser ? 'right' : '')}>
                <div className="direct-chat-info clearfix">
                    <span className={"direct-chat-name pull-" + (sameUser ? 'right' : 'left')}>{ this.props.message.user_email }</span>
                    <span className={"direct-chat-timestamp pull-" + (sameUser ? 'left' : 'right')}>{this.props.message.built_at}</span>
                </div>
                <img className="direct-chat-img" src="/assets/user1-128x128.jpg" alt="Message User Image" />

                <div className="direct-chat-text">
                    { this.props.message.content }
                </div>
            </div>
        )
    }
}
