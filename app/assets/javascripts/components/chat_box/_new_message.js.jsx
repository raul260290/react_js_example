class NewMessage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentMessage: ''
        };
    }


    /*updateCurrentMessage(event) {
        let content = this.refs.content.value;
        this.setState({
            currentMessage: content
        });
    }*/

    updateCurrentChatMessage(event) {
        this.setState({
            currentMessage: event.target.value
        })
    }

    handleSendEvent(event) {
        event.preventDefault();
        let content = this.state.currentMessage;
        if (content !== '') {
            this.props.chats.create(content);
            this.setState({ currentMessage: '' })
        }
    }

    handleChatInputKeyPress(event) {
        if (event.key === 'Enter')
            this.handleSendEvent(event);
    }

    render() {
        return(
            <div className="box-footer">
                <form action="#" method="post">
                    <div className="input-group">
                        <input
                            value={this.state.currentMessage}
                            onKeyPress={ (e) => this.handleChatInputKeyPress(e) }
                            onChange={ (e) => this.updateCurrentChatMessage(e) }
                            type="text"
                            name="message"
                            placeholder="Type Message ..."
                            className="form-control" />
                        <span className="input-group-btn">
                            <button
                                onClick={ (e) => this.handleSendEvent(e) }
                                type="submit"
                                className="btn btn-success btn-flat">
                                Send
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        )
    }
}
