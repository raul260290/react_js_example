class HeaderChatBox extends React.Component {
    render() {
        return (
            <div className="box-header with-border">
                <h3 className="box-title">Direct Chat</h3>

                <div className="box-tools pull-right">
                    <span data-toggle="tooltip" title="" className="badge bg-green" data-original-title="3 New Messages">3</span>
                    <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-plus"></i>
                    </button>
                    <button type="button" className="btn btn-box-tool" data-toggle="tooltip" title="" data-widget="chat-pane-toggle" data-original-title="Contacts">
                        <i className="fa fa-comments"></i></button>
                    <button type="button" className="btn btn-box-tool" data-widget="remove"><i className="fa fa-times"></i></button>
                </div>
            </div>
        )
    }
}
