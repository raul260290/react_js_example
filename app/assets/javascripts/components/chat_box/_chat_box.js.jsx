//= require action_cable

class ChatBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: []
        }
    }

    renderChatLog() {
        return this.state.messages.map((el) => {
            return(
                <div key={el.id}>
                    <Message message={el} userId={this.props.userId} />
                </div>
            )
        });
    }

    createSocket () {
        let userId = this.props.userId;
        let cable = ActionCable.createConsumer();
        this.chats = cable.subscriptions.create({
            channel: 'ChatChannel'
        }, {
            connnected: () => {},
            received: (data) => {
                let messages = this.state.messages;
                messages.push(JSON.parse(data));
                this.setState({ messages: messages });
                this.scrollToBottom();
            },
            create: function(chatContent) {
                this.perform('create', {
                    content: chatContent,
                    user_id: userId
                });
            }
        });
    }

    scrollToBottom() {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }

    componentWillMount() {
        this.createSocket();
        $.getJSON(
            '/api/v1/messages.json',
            response =>  {
                console.log(response);
                this.setState({ messages: response })
            }
        );
    }

    componentDidMount() {
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        return(
            <div className="box box-success direct-chat direct-chat-success collapsed-box">
                <HeaderChatBox />
                <div className="box-body">
                    <div className="direct-chat-messages">
                        <div className="list">
                            { this.renderChatLog() }
                        </div>
                        <div style={{ float:"left", clear: "both" }}
                             ref={(el) => { this.messagesEnd = el; }}>
                        </div>
                    </div>
                    <NewMessage chats={this.chats} />
                </div>
            </div>
        )
    }
}

let chatBox = document.getElementById('chat-box');

if (chatBox !== null) {
    console.log('hey!');
    let userId = parseInt(chatBox.dataset.userId);
    ReactDOM.render(<ChatBox userId={userId} />, chatBox);
}
