let Body = React.createClass({
    getInitialState() {
        return { items: [] }
    },

    componentDidMount() {
        $.getJSON('/api/v1/items.json', response => { this.setState({ items: response }) })
    },

    handleSubmit(item) {
        let newState = this.state.items.concat(item);
        this.setState({ items: newState });
    },

    handleDelete(id) {
        $.ajax({
            url: 'api/v1/items/' + id,
            type: 'DELETE',
            success: () => {
                this.removeItemClient(id);
                console.log('successfully remove item');
            }
        });
        console.log('in handle delete')
    },

    removeItemClient(id) {
        let newItems = this.state.items.filter((item) => {
            return item.id !== id;
        });

        this.setState({ items: newItems });
    },

    handleUpdate(item) {
        $.ajax({
            url: '/api/v1/items/' + item.id,
            type: 'PUT',
            data: { item: item },
            success: () => {
                console.log('you did it!!!');
                this.updateItem(item)
            }
        })
    },

    updateItem(item) {
        let items = this.state.items.filter((i) => { return i.id !== item.id });
        items.push(item);

        this.setState({ items: items });
    },

    render() {
        return (
            <div>
                <NewItem handleSubmit={this.handleSubmit} />
                <AllItems items={this.state.items} handleDelete={this.handleDelete} onUpdate={this.handleUpdate} />
            </div>
        )
    }
});
