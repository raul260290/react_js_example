//= require action_cable

class ChatMain extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currentChatMessage: '',
            chatLogs: []
        };
    }

    updateCurrentChatMessage(event) {
        this.setState({
            currentChatMessage: event.target.value
        })
    }

    createSocket() {
        let cable = ActionCable.createConsumer();
        this.chats = cable.subscriptions.create({
            channel: 'ChatChannel'
        }, {
            connected: () => {},
            received: (data) => {
                let chatLogs = this.state.chatLogs;
                chatLogs.push(data);
                console.log(data);
                this.setState({ chatLogs: chatLogs })
            },
            create: function(chatContent) {
                this.perform('create', {
                    content: chatContent
                })
            }
        });
    }

    componentWillMount() {
        this.createSocket();
        /*
        let number = 1000;
        var cont = 0;
        while (cont < number) {
            let chatLogs = this.state.chatLogs;
            chatLogs.push({id: cont, content: cont, built_on: '27 Ago 2017 8:56'});
            this.setState({ chatLogs: chatLogs });
            cont += 1;
        }
        */
    }

    handleSendEvent(event) {
        event.preventDefault();
        this.chats.create(this.state.currentChatMessage);
        this.setState({
            currentChatMessage: ''
        });

    }

    renderChatLog() {
        return this.state.chatLogs.map((el) => {
            return (
                <li key={`chat_${el.id}`}>
                    <span className="chat-message">{ el.content }</span> -
                    <span className="chat-created-at">{ el.built_at }</span>
                </li>
            );
        });
    }

    handleChatInputKeyPress(event) {
        if (event.key === 'Enter') {
            this.handleSendEvent(event)
        }
    }

    render() {
        return(
            <div className="ChatApp">
                <div className="stage">
                    <h1>Chat</h1>
                    <div className="chat-logs">
                        { this.renderChatLog() }
                    </div>
                    <input value={this.state.currentChatMessage}
                           onKeyPress={ (e) => this.handleChatInputKeyPress(e) }
                           onChange={ (e) => this.updateCurrentChatMessage(e) }
                           type="text"
                           placeholder="Enter your message..."
                           className="chat-input"
                    />
                    <button onClick={ (e) => this.handleSendEvent(e) }
                            className="send">
                        Send
                    </button>
                </div>
            </div>
        )
    }
}

let rootChat = document.getElementById('root');
if (rootChat !== null) {
    ReactDOM.render(<ChatMain />, rootChat);
}
