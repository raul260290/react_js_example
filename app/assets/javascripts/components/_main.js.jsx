var Main = React.createClass({
    render() {
        return (
            <div>
                <Header />
                <Body />
            </div>
        )
    }
});

let main = document.getElementById('main');
if (main !== null)
    ReactDOM.render(<Main />, main);

