class Api::V1::MessagesController < Api::V1::BaseController
  def index
    respond_with ChatMessage.
        order(created_at: :desc).
        limit(20).
        reverse.to_json(only: [:id, :content, :built_at, :user_id, :user_email], methods: [:built_at, :user_email])
  end

  def create
    respond_with :api, :v1, ChatMessage.create(chat_message_params)
  end

  def update
    chat_message = ChatMessage.find(params[:id])
    chat_message.update_attributes(chat_message_params)
    respond_with chat_message, json: chat_message
  end

  def destroy
    respond_with ChatMessage.destroy(params[:id])
  end

  private
  def chat_message_params
    params.require(:chat_message).permit(:content)
  end
end
