class ChatMessage < ApplicationRecord

  belongs_to :user
  delegate :email, to: :user, prefix: true

  after_create_commit do
    ChatMessageCreationJob.perform_later(self)
  end

  def built_at
    I18n.l(self.created_at, format: :custom)
  end

end
