Rails.application.routes.draw do
  devise_for :users
  mount ActionCable.server => '/cable'

  get 'chat' => 'site#chat', as: :chat
  get 'simple_chat' => 'site#simple_chat', as: :simple_chat
  get 'chat_layout' => 'site#chat_layout', as: :chat_layout

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :items, only: [:index, :create, :update, :destroy]
      resources :messages, only: [:index, :create, :update, :destroy]
    end
  end

  root to: 'site#index'

end
